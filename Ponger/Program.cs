﻿using System;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Wrapper.Domain.Interface;
using RabbitMQ.Wrapper.Domain;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client.Events;
using System.Text;
using RabbitMQ.Client;

namespace Ponger
{
    class Program
    {
        public static IWrapper mqWrapper { get; set; }
        public static IConfiguration configuration { get; set; }
        static void Receive(string message)
        {
            Console.WriteLine($"{message} {DateTime.Now}");
            Thread.Sleep(int.Parse(configuration.GetSection("Time").Value));
            Respond("pong");
        }
        static void Respond(string message)
        {
            mqWrapper.SendMessageToQueue(message, "ping_queue");
        }
        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
         .AddJsonFile($"{Directory.GetCurrentDirectory()}/appsettings.json", optional: false, reloadOnChange: true)
         .AddEnvironmentVariables()
         .AddCommandLine(args)
         .Build();
            mqWrapper = new PingPongQueue(configuration);
            mqWrapper.ListenQueue(Receive, "pong_queue");
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadKey();
            mqWrapper.Dispose();
        }
    }
}

﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Wrapper.Domain.Interface;
using RabbitMQ.Wrapper.Domain;
using System.Threading;
namespace Pinger
{
    class Program
    {
        public static IWrapper mqWrapper { get; set; }
        public static IConfiguration configuration { get; set; }
        static void Receive(string message)
        {
            Console.WriteLine($"{message} {DateTime.Now}");
            Thread.Sleep(int.Parse(configuration.GetSection("Time").Value));
            Respond("ping");
        }
        static void Respond(string message)
        {
            mqWrapper.SendMessageToQueue(message, "pong_queue");
        }
        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
                .AddJsonFile($"{Directory.GetCurrentDirectory()}/appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            mqWrapper = new PingPongQueue(configuration);
            mqWrapper.SendMessageToQueue("ping", "pong_queue");
            mqWrapper.ListenQueue(Receive, "ping_queue");
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadKey();
            mqWrapper.Dispose();
        }
    }

}

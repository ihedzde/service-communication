using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Domain.Interface;

namespace RabbitMQ.Wrapper.Domain
{
    public class PingPongQueue : IWrapper
    {
        private readonly IConfiguration _config;
        private readonly IConnectionFactory _factory;
        private IConnection _connection;
        public PingPongQueue(IConfiguration configuration)
        {
            _config = configuration;
            _factory = new ConnectionFactory()
            {
                HostName = _config.GetSection("RabbitMQ")["HostName"],
                Port = int.Parse(_config.GetSection("RabbitMQ")["Port"]),
                UserName = _config.GetSection("RabbitMQ").GetSection("Auth")["UserName"],
                Password = _config.GetSection("RabbitMQ").GetSection("Auth")["Password"],
            };
            _connection = _factory.CreateConnection();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public void ListenQueue(Action<string> method, string queue)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    method.Invoke(message);
                };
                channel.BasicConsume(queue: queue,
                                     autoAck: true,
                                     consumer: consumer);
                Console.ReadKey();
            }
        }

        public void SendMessageToQueue(string message, string queue)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: queue,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
;
                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: queue,
                                     basicProperties: properties,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
        }
    }
}
using System;

namespace RabbitMQ.Wrapper.Domain.Interface
{
    public interface IWrapper: IDisposable
    {
         void ListenQueue(Action<string> method, string queue);
         void SendMessageToQueue(string message, string queue);
    }
}